package com.emad.hichtest

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.pow
import kotlin.math.sign
import kotlin.math.sqrt

class MainActivity : Activity() {

    private val genTolRaw = 50 //coin popping randomized distance tolerance in DP
    private var genTol = 0F //temp value to obtain Resources
    private val viewDiameterRaw = 30 //coins diameters in DP
    private var viewDiameter = 0F //temp value to obtain Resources
    private val magnifier = 1000 //used to generate random numbers up to 1000
    private val baseId = 256 //base number for manual view attribution
    private val maxCoins = 50 //max number of coins possible
    private val dragTime = 600L //time for a coin to drag to top left and exit the screen
    private val popTime = 75L //time for a coin to pop around the center circle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewDiameter = (viewDiameterRaw * resources.displayMetrics.density + 0.5f) //delayed calculation of coin diameter in PX
        genTol = (genTolRaw * resources.displayMetrics.density + 0.5f) //delayed calculation of genTolin PX
        bGold.setOnClickListener {
            rlRoot.removeAllViews() //every view is garbage collected at animation finish, this ensures memory emptying up from animator objects
            rlRoot.addView(bGold) //center button is always present

            val animation = AnimationUtils.loadAnimation(this, R.anim.sizebounce) //bounce effect on center button
            bGold.startAnimation(animation)

            val coinCount = (((Math.random() * magnifier).toInt()) % maxCoins) + 1 //making sure there never is zero coins
            for (i in 0 until coinCount)
                makeCoins()
        }
    }

    private fun makeCoins() {
        val newView = View(this) //making a coin
        newView.alpha = 0F
        // putting the new view at the center of screen
        val initX = resources.displayMetrics.widthPixels / 2 - viewDiameter / 2
        val initY = resources.displayMetrics.heightPixels / 2 - viewDiameter / 2
        newView.layoutParams = ViewGroup.LayoutParams(viewDiameter.toInt(), viewDiameter.toInt())
        newView.x = initX
        newView.y = initY
        newView.id = baseId + rlRoot.childCount
        newView.background = resources.getDrawable(R.drawable.generic_circle) //hardware layering wasn't used because views are simple. cached views can be used for an actual coin image
        val newX = randomizeNearButton(Orientation.X) //generating a random x close to center button
        val rawY = sqrt(
            genTol.pow(2) - (newX - bGold.x - bGold.width / 2 + viewDiameter / 2).pow(2) //coins form a circle around middle button
        )
        val finalY =
            (Math.random() - 0.5).sign * rawY + bGold.y + bGold.height / 2 - viewDiameter / 2
        val newY = finalY.toFloat()
        rlRoot.addView(newView)
        newView.animate()
            .setDuration(popTime)
            .translationX(newX) //moving to new designated position on the circle individually
            .translationY(newY)
            .setInterpolator(DecelerateInterpolator())
            .alpha(1F) //appearing effect of coins
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(p0: Animator?) {
                }

                override fun onAnimationCancel(p0: Animator?) {
                }

                override fun onAnimationStart(p0: Animator?) {
                }

                override fun onAnimationEnd(p0: Animator?) {
                    animateCoinToTopLeft(newView.id) //each coin starts next animation immediately
                }
            }).setStartDelay(10 * rlRoot.childCount.toLong()) //make a sense of collection instead of raw movement
            .start()
    }

    private fun animateCoinToTopLeft(id: Int) {
        var view: View? = findViewById(id) ?: return //making sure an invalid id isn't being found
        view!!.animate()
            .setDuration(dragTime)
            .translationY(-viewDiameter) //coins move to just outside the screen
            .translationX(-viewDiameter)
            .setInterpolator(AccelerateDecelerateInterpolator()) //used for reaching effect
            .setListener(object: Animator.AnimatorListener{
                override fun onAnimationRepeat(p0: Animator?) {
                }

                override fun onAnimationEnd(p0: Animator?) {
                    rlRoot.removeView(view) //view is removed for garbage collecting immediately
                }

                override fun onAnimationCancel(p0: Animator?) {
                }

                override fun onAnimationStart(p0: Animator?) {
                }
            })
            .start()
    }

    private fun randomizeNearButton(o: Orientation): Float { //this function is generalized to be able to work for other formations than circle (for future features)
        val tolerance = 2 * (((Math.random() * magnifier) % genTol).toFloat() - genTol / 2) //generation tolerance factor (genTol) determines how far coins pop
        return when (o) {
            Orientation.X -> bGold.x + bGold.width / 2 - viewDiameter / 2 + tolerance
            Orientation.Y -> bGold.y + bGold.height / 2 - viewDiameter / 2 + tolerance
        }
    }

    private enum class Orientation {
        X, Y
    }
}

